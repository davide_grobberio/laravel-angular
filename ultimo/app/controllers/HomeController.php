<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
  		$this->layout->content = View::make('benvenuto');
	}

	public function login()
	{
 		$this->layout->content = View::make('login.form_login');
	}

	public function login_process()
	{
 
		$userdata = array(
			'username'  => Input::get('username')
			,'password' => Input::get('password')
			);
		 
		if( Auth::attempt($userdata) ){
			Session::put('username', $userdata['username']);

			if (Auth::user()->livello == 0 ){
				return Redirect::to('area_utenti');
			}else{
				return Redirect::to('area_risorse_utenti');
			}
		}else{
			return Redirect::to('login');
		}
	}

	public function area_utenti()
	{ 
		$this->layout->content = View::make('area.main');
	}

	public function area_risorse_utenti()
	{
		// recuperiamo le categorie appartenenti all'utente
		$data['categorie'] = explode( ',',Auth::user()->categorie);

		// recuperiamo tutte le risorse
		$data['risorse'] = Risorse::all();

		$this->layout->content = View::make('area.main_utenti', $data); 
	} 
}