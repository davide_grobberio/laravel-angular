<?php

class TagController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
 		$data['tag_lista'] = Tag::all();
		$this->layout->content = View::make('tag.index', $data);		 

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
 		$this->layout->content = View::make('tag.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = array(
			'nome_tag' => Input::get('nome_tag')
		);

		$regole = array(
			'nome_tag' => 'required'
		);

		$validatore = Validator::make($data,$regole);

		if( $validatore->passes() ){
	 		$tag = new Tag;
			$tag->nome_tag = $data['nome_tag'];
			$tag->save();
			return Redirect::action('TagController@index');
		}else{
 			 return Redirect::action('TagController@create')->withInput(); 
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['tag_dettaglio'] = Tag::find($id);
		$this->layout->content = View::make('tag.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['tag_dettaglio'] = Tag::find($id);
		$this->layout->content = View::make('tag.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
			'nome_tag' => Input::get('nome_tag')
 
		);

		$regole = array(
			'nome_tag' => 'required'
 
		);

		$validatore = Validator::make($data,$regole);		
		 
		if( $validatore->passes() ){
			 $tag = Tag::find($id);
			 $tag->nome_tag = $data['nome_tag'];
			 $tag->save();
			 return Redirect::action('TagController@index');
		}else{
			return Redirect::action('TagController@edit', [$id])->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	 $tag = Tag::find($id);
 	 $tag->delete();
	 return Redirect::action('TagController@index'); 	
	}

}