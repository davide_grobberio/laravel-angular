@section('content')
<div class="row">
	<div class="col-lg-12">
		<span class="pull-right">
			<a href="{{ url('utenti/create') }}" class="btn btn-success">
				Nuovo utente
			</a>
		</span>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped">
		<tr class="success">
			<td>Utente</td>
			<td>Azioni</td>
			<td>Ruolo</td>
		</tr>
		@foreach($utenti_lista as $c)
		<tr>
			<td>{{ $c['username'] }}</td>
			<td>@if ($c['livello'] == 0)
				Admin
				@else
				Utente Base
				@endif			
			</td>
			<td>
			<a href="{{ url('utenti/'.$c['id'].'/edit') }}" class="btn btn-warning">Modifica</a> 
			<a href="{{ url('utenti/'.$c['id']) }}" class="btn btn-warning">Cancella</a>
			</td>
		</tr>
		@endforeach
		</table>
	</div>
</div>
@stop