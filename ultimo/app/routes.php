<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {
// 	return View::make('hello');
// });

Route::get('/', 'HomeController@showWelcome');

Route::get('login', 'HomeController@login'); 

Route::post('login_process', array(
							'before' => 'csrf',  
							'uses' => 'HomeController@login_process'
							));

Route::get('area_utenti', array(
 							'before' => 'auth|livello_uno',
 							'uses' => 'HomeController@area_utenti'
 							));

Route::get('area_risorse_utenti', array(
 							'before' => 'auth',
 							'uses' => 'HomeController@area_risorse_utenti'
 							));

Route::get('logout', function(){
		Auth::logout();		
		Session::flush();
		return Redirect::to('login');
});
 
Route::group(array('before'=>'auth|livello_uno'), function() {   
    	Route::resource('categorie', 'CategorieController');   
    	Route::resource('utenti', 'UtentiController');  
    	Route::resource('risorse', 'RisorseController');
    	Route::resource('links', 'LinksController');
    	Route::resource('tag', 'TagController');
});