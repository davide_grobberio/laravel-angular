<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Utenti extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'utenti';
	public $timestamps = FALSE;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');
 
 	public static function boot()
 	{
 		static::saved(function()
 		{
 			Event::fire('utente.aggiorna');
 		});

 		static::deleted(function()
 		{
 			Event::fire('utente.aggiorna');
 		});

 	}

	public static function TrovaOLanciaEccezione($id)
	{
		$user = static::find($id);
		if ( ! is_null($user)) return $user;
		throw new ModelNonTrovatoEccezione("Il record non esiste");
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

}