// _____________________________________ COMMENT CONTROLLER
angular.module('mainCtrl', [])

	.controller('mainController', function($scope, $http, Comment) {
		// object to hold all the data for the new comment form
		$scope.commentData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;

		// get all the comments first and bind it to the $scope.comments object
		Comment.get()
			.success(function(data) {
				$scope.comments = data;
				$scope.loading = false;
			});


		// function to handle submitting the form
		$scope.submitComment = function() {
			$scope.loading = true;

			// save the comment. pass in comment data from the form
			Comment.save($scope.commentData)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Comment.get()
						.success(function(getData) {
							$scope.comments = getData;
							$scope.loading = false;
						});

				})
				.error(function(data) {
					console.log(data);
				});
		};

		// function to handle deleting a comment
		$scope.deleteComment = function(id) {
			$scope.loading = true;

			Comment.destroy(id)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Comment.get()
						.success(function(getData) {
							$scope.comments = getData;
							$scope.loading = false;
						});

				});
		};

	});


// _____________________________________ CATEGORY CONTROLLER
angular.module('mainCategoryCtrl', [])

	.controller('mainCategoryController', function($scope, $http, Category) {
		// object to hold all the data for the new categorie form
		$scope.categoryData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;

		// get all the categorie first and bind it to the $scope.categorie object
		Category.get()
			.success(function(data) {
				$scope.categorie = data;
				$scope.loading = false;
			});


		// function to handle submitting the form
		$scope.submitCategory = function() {
			$scope.loading = true;

			// save the category. pass in category data from the form
			Category.save($scope.categoryData)
				.success(function(data) {
					// if successful, we'll need to refresh the categorie list
					Category.get()
						.success(function(getData) {
							$scope.categorie = getData;
							$scope.loading = false;
						});
				})
				.error(function(data) {
					console.log(data);
				});
		};

		// function to show single category
		$scope.showCategory = function(id) {
			$scope.loading_cat = true;

			Category.show(id)
				.success(function(data) {
					// if successful, we'll need to refresh the categorie list
							$scope.singlecategory = data;
							$scope.loading_cat = false;
				})
				.error(function(data) {
					console.log(data);
				});
		};

		// function to handle deleting a categorie
		$scope.updateCategory = function(id) {
			$scope.loading = true;

			Category.update(id, $scope.singlecategory)
				.success(function(data) {
					// if successful, we'll need to refresh the categorie list
					Category.get()
						.success(function(getData) {
							$scope.categorie = getData;
							$scope.loading = false;
						});
				});
		};

		// function to handle deleting a categorie
		$scope.deleteCategory = function(id) {
			$scope.loading = true;

			Category.destroy(id)
				.success(function(data) {
					// if successful, we'll need to refresh the categorie list
					Category.get()
						.success(function(getData) {
							$scope.categorie = getData;
							$scope.loading = false;
						});
				});
		};

	});


// _____________________________________ POST CONTROLLER
angular.module('mainPostCtrl', [])

	.controller('mainPostController', function($scope, $http, Post) {
		// object to hold all the data for the new post form
		$scope.postData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;
		$scope.alert = {
			display : false,
			message : ''
		};

		// get all the posts first and bind it to the $scope.posts object
		Post.get()
			.success(function(data) {
				$scope.posts = data;
				$scope.loading = false;
			});


		// function to handle submitting the form
		$scope.submitPost = function() {
			$scope.loading = true;

			// save the post. pass in post data from the form
			Post.save($scope.postData)
				.success(function(data) {

					Post.get()
						.success(function(getData) {
							$scope.posts = getData;
							$scope.loading = false;
							$scope.alert = {
								display : true,
								message : 'Ottimo, post creato!'
							}
							$scope.postData = {};
						});

				})
				.error(function(data) {
					console.log(data);
				});
		};

		$scope.showPost = function(id) {
			$scope.loading_cat = true;

			Post.show(id)
				.success(function(data) {

							$scope.singlepost = data;
							$scope.loading_cat = false;
				})
				.error(function(data) {
					console.log(data);
				});
		};

		$scope.updatePost = function(id) {
			$scope.loading = true;

			Post.update(id, $scope.singlepost)
				.success(function(data) {
					Post.get()
						.success(function(getData) {
							$scope.posts = getData;
							$scope.loading = false;
							$scope.alert = {
								display : true,
								message : 'Bene, post modificato!'
							}
							$scope.singlepost = {};
						});
				});
		};

		// function to handle deleting a post
		$scope.deletePost = function(id) {
			$scope.loading = true;

			Post.destroy(id)
				.success(function(data) {

					// if successful, we'll need to refresh the post list
					Post.get()
						.success(function(getData) {
							$scope.posts = getData;
							$scope.loading = false;
							$scope.alert = {
								display : true,
								message : 'Post cancellato con successo!'
							}
						});

				});
		};

	});



// _____________________________________ TAG CONTROLLER
	angular.module('mainTagCtrl', [])

		.controller('mainTagController', function($scope, $http, Tag) {
			// object to hold all the data for the new tag form
			$scope.tagData = {};

			// loading variable to show the spinning loading icon
			$scope.loading = true;
			$scope.alert = {
				display : false,
				message : ''
			};

			// get all the tags first and bind it to the $scope.tags object
			Tag.get()
				.success(function(data) {
					$scope.tags = data;
					$scope.loading = false;
				});


			// function to handle submitting the form
			$scope.submitTag = function() {
				$scope.loading = true;

				// save the tag.
				Tag.save($scope.tagData)
					.success(function(data) {

						Tag.get()
							.success(function(getData) {
								$scope.tags = getData;
								$scope.loading = false;
								$scope.alert = {
									display : true,
									message : 'Ottimo, tag creato!'
								}
								$scope.tagData = {};
							});

					})
					.error(function(data) {
						console.log(data);
					});
			};

			$scope.showTag = function(id) {
				$scope.loading = true;

				Tag.show(id)
					.success(function(data) {

								$scope.singletag = data;
								$scope.loading = false;
								$scope.alert = {
									display : true,
									message : 'Ottimo, tag caricato!'
								}
					})
					.error(function(data) {
						console.log(data);
					});
			};

			$scope.updateTag = function(id) {
				$scope.loading = true;

				Tag.update(id, $scope.singletag)
					.success(function(data) {
						Tag.get()
							.success(function(getData) {
								$scope.tags = getData;
								$scope.loading = false;
								$scope.alert = {
									display : true,
									message : 'Bene, tag modificato!'
								}
								$scope.singletag = {};
							});
					});
			};

			// function to handle deleting a post
			$scope.deleteTag = function(id) {
				$scope.loading = true;

				Tag.destroy(id)
					.success(function(data) {

						Tag.get()
							.success(function(getData) {
								$scope.tags = getData;
								$scope.loading = false;
								$scope.alert = {
									display : true,
									message : 'Tag cancellato con successo!'
								}
							});

					});
			};

		});



		// _____________________________________ LINKS CONTROLLER
		angular.module('mainLinksCtrl', [])

		.controller('mainLinksController', function($scope, $http, Links) {
			// object to hold all the data for the new links form
			$scope.linksData = {};

			// loading variable to show the spinning loading icon
			$scope.loading = true;
			$scope.alert = {
				display : false,
				message : ''
			};

			// get all the links first and bind it to the $scope.links object
			Links.get()
			.success(function(data) {
				$scope.links = data;
				$scope.loading = false;
			});


			// function to handle submitting the form
			$scope.submitLink = function() {
				$scope.loading = true;

				// save the link.
				Links.save($scope.linksData)
				.success(function(data) {

					Links.get()
					.success(function(getData) {
						$scope.links = getData;
						$scope.loading = false;
						$scope.alert = {
							display : true,
							message : 'Ottimo, link creato!'
						}
						$scope.linksData = {};
					});

				})
				.error(function(data) {
					console.log(data);
				});
			};

			$scope.showLink = function(id) {
				$scope.loading = true;

				Links.show(id)
				.success(function(data) {

					$scope.singlelink = data;
					$scope.loading = false;
					$scope.alert = {
						display : true,
						message : 'Ottimo, link caricato!'
					}
				})
				.error(function(data) {
					console.log(data);
				});
			};

			$scope.updateLink = function(id) {
				$scope.loading = true;

				Links.update(id, $scope.singlelink)
				.success(function(data) {
					Links.get()
					.success(function(getData) {
						$scope.links = getData;
						$scope.loading = false;
						$scope.alert = {
							display : true,
							message : 'Bene, link modificato!'
						}
						$scope.singlelink = {};
					});
				});
			};

			// function to handle deleting a post
			$scope.deleteLink = function(id) {
				$scope.loading = true;

				Links.destroy(id)
				.success(function(data) {

					Links.get()
					.success(function(getData) {
						$scope.links = getData;
						$scope.loading = false;
						$scope.alert = {
							display : true,
							message : 'Link cancellato con successo!'
						}
					});

				});
			};

		});




// _____________________________________ UTENTI CONTROLLER
	angular.module('mainUtentiCtrl', [])

		.controller('mainUtentiController', function($scope, $http, Utenti) {

			$scope.utentiData = {};

			$scope.loading = true;

			// get all the categorie first and bind it to the $scope.users object
			Utenti.get()
				.success(function(data) {
					$scope.users = data;
					$scope.loading = false;
				});

			// function to handle submitting the form
			$scope.submitUtenti = function() {
				$scope.loading = true;

				// save the user. pass in category data from the form
				Utenti.save($scope.utentiData)
					.success(function(data) {
						// if successful, we'll need to refresh the utenti list
						Utenti.get()
							.success(function(getData) {
								$scope.users = getData;
								$scope.loading = false;
							});
					})
					.error(function(data) {
						console.log(data);
					});
			};

			// function to show single category
			$scope.showUtenti = function(id) {
				$scope.loading_cat = true;

				Utenti.show(id)
					.success(function(data) {
								$scope.singleuser = data;
								$scope.loading_cat = false;
					})
					.error(function(data) {
						console.log(data);
					});
			};

			// function to handle deleting a categorie
			$scope.updateUtente = function(id) {
				$scope.loading = true;

				Utenti.update(id, $scope.singlecategory)
					.success(function(data) {
						Utenti.get()
							.success(function(getData) {
								$scope.users = getData;
								$scope.loading = false;
							});
					});
			};

			// function to handle deleting a categorie
			$scope.deleteUtente = function(id) {
				$scope.loading = true;

				Utenti.destroy(id)
					.success(function(data) {
						Utenti.get()
							.success(function(getData) {
								$scope.users = getData;
								$scope.loading = false;
							});
					});
			};

		});
