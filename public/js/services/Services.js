angular.module('categoryService', [])

	.factory('Category', function($http) {

		return {
			get : function() {
				return $http.get('/api/categorie');
			},
			show : function(id) {
				return $http.get('/api/categorie/' + id);
			},
			save : function(categoryData) {
				return $http({
					method: 'POST',
					url: '/api/categorie',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(categoryData)
				});
			},
			update : function(singlecategory) {
				return $http({
					method: 'PUT',
					url: '/api/categorie/' + singlecategory.id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(singlecategory)
				});
			},
			destroy : function(id) {
				return $http.delete('/api/categorie/' + id);
			}
		}

	});


	angular.module('postService', [])

	.factory('Post', function($http) {

		return {
			get : function() {
				return $http.get('/api/posts');
			},
			show : function(id) {
				return $http.get('/api/posts/' + id);
			},
			save : function(postData) {
				return $http({
					method: 'POST',
					url: '/api/posts',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(postData)
				});
			},
			update : function(singlepost) {
				return $http({
					method: 'PUT',
					url: '/api/posts/' + singlepost.id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(singlepost)
				});
			},
			destroy : function(id) {
				return $http.delete('/api/posts/' + id);
			}
		}

	});

	angular.module('tagService', [])

	.factory('Tag', function($http) {

		return {
			get : function() {
				return $http.get('/api/tags');
			},
			show : function(id) {
				return $http.get('/api/tags/' + id);
			},
			save : function(tagData) {
				return $http({
					method: 'POST',
					url: '/api/tags',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(tagData)
				});
			},
			update : function(singletag) {
				return $http({
					method: 'PUT',
					url: '/api/tags/' + singletag.id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(singletag)
				});
			},
			destroy : function(id) {
				return $http.delete('/api/tags/' + id);
			}
		}

	});


	angular.module('commentService', [])

	.factory('Comment', function($http) {

		return {
			get : function() {
				return $http.get('/api/comments');
			},
			show : function(id) {
				return $http.get('/api/comments/' + id);
			},
			save : function(commentData) {
				return $http({
					method: 'POST',
					url: '/api/comments',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(commentData)
				});
			},
			destroy : function(id) {
				return $http.delete('/api/comments/' + id);
			}
		}

	});

	angular.module('linksService', [])

	.factory('Links', function($http) {

		return {
			get : function() {
				return $http.get('/api/links');
			},
			show : function(id) {
				return $http.get('/api/links/' + id);
			},
			save : function(linksData) {
				return $http({
					method: 'POST',
					url: '/api/links',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(tagData)
				});
			},
			update : function(singlelink) {
				return $http({
					method: 'PUT',
					url: '/api/links/' + singlelink.id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(singletag)
				});
			},
			destroy : function(id) {
				return $http.delete('/api/links/' + id);
			}
		}

	});


	angular.module('utentiService', [])

		.factory('Utenti', function($http) {

			return {
				get : function() {
					return $http.get('/api/utenti');
				},
				show : function(id) {
					return $http.get('/api/utenti/' + id);
				},
				save : function(userData) {
					return $http({
						method: 'POST',
						url: '/api/utenti',
						headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
						data: $.param(categoryData)
					});
				},
				update : function(singleuser) {
					return $http({
						method: 'PUT',
						url: '/api/utenti/' + singleuser.id,
						headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
						data: $.param(singleuser)
					});
				},
				destroy : function(id) {
					return $http.delete('/api/utenti/' + id);
				}
			}

		});
