<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRisorse extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('risorse', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titolo_risorsa');
			$table->mediumText('descrizione_risorsa');
			$table->string('url_risorsa');
			$table->string('categorie');			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('risorse');
	}

}
