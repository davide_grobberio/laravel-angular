<?php

class LinksTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('links')->truncate();

		$links = array(
			'nome_link' => 'Google'
			,'url' => 'http://www.google.it'
		);

		// Uncomment the below to run the seeder
		 DB::table('link')->insert($links);
	}

}
