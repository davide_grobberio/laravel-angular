<!doctype html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<title>Laravel and Angular Comment System</title>

	<!-- CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> <!-- load bootstrap via cdn -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
	<link rel="stylesheet" href="{{ url('css/style.css') }}">

	<!-- JS -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script> <!-- load angular -->

	<!-- ANGULAR -->
	<!-- all angular resources will be loaded from the /public folder -->
	<script src="{{ url('js/controllers/mainCtrl.js') }}"></script>
	<script src="{{ url('js/services/commentService.js') }}"></script>
	<script src="{{ url('js/controllers/mainPostCtrl.js') }}"></script>
	<script src="{{ url('js/services/postService.js') }}"></script>
	<script src="{{ url('js/controllers/mainNewCtrl.js') }}"></script>
	<script src="{{ url('js/services/newService.js') }}"></script>
	<script src="{{ url('js/app.js') }}"></script> <!-- load our application -->

</head>
<!-- declare our angular app and controller -->
<body>

	 @yield('content')

</body>
</html>