@section('content')

{{ Form::open(array('url' => 'categorie', 'method' => 'POST')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('nome_categoria', 'Nome categoria') }}
		{{ Form::text('nome_categoria', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
		{{ Form::label('descrizione', 'Descrizione') }}
		{{ Form::text('descrizione', '', array('class'=>'form-control')) }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiungi questa categoria',  array('class' =>'btn btn-success btn-large')) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop