@section('content')
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
		<h3>Sei sicuro di voler cancellare questo tag?</h3>
		</div>
	</div>
</div> 

{{ Form::open(array('url' => 'tag/'. $tag_dettaglio->id, 'method' => 'DELETE')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			 {{ $tag_dettaglio->nome_tag }}
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Cancella questo tag',  array('class' =>'btn btn-success btn-large')) }}
			
		</div>
	</div>

	<div class="col-lg-3">
		<a href="{{ url('tag') }}" class="btn btn-warning btn-large">No, ci ho ripensato</a>
	</div>
</div>
{{ Form::close() }}
@stop