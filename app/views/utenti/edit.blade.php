@section('content')
{{ Form::open(array('url' => 'utenti/' . $utenti_dettaglio->id, 'method' => 'PUT')) }}
<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::label('username', 'Username') }}
			{{ Form::text('username', $utenti_dettaglio->username, array('class' => 'form-control')) }}
		</div>
	</div>
</div>

<div clas="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::label('password', 'Password (lascia vuoto se non vuoi modificare)') }}
			{{ Form::password('password', array('class' => 'form-control')) }}
		</div>
	</div>
</div>

<div clas="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::label('livello', 'Livello utente') }}
			{{ Form::select('livello', $livelli, $utenti_dettaglio->livello ) }}
		</div>
	</div>
</div>

<div clas="row">
	<div class="col-lg-3">
		<div class="form-group">
			@foreach($categorie_lista as $c)
				@if (in_array($c['id'], $categorie_scelte))
					<?php $vero_falso = true ?>
				@else
					<?php $vero_falso = false ?>
				@endif

				<div class="checkbox">
					<label>
						{{ Form::checkbox('categorie[]', $c['id'], $vero_falso) }}
						{{ $c['nome_categoria'] }}
					</label>
				</div>
			@endforeach
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3">
		<div class="form-group">
			{{ Form::submit('Aggiorna questo utente', array('class' => 'btn btn-success btn-large' )) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop
