@section('content')
<div class="container" ng-app="utentiApp" ng-controller="mainUtentiController">

		<div class="row">
				<div class="page-header">
					<h2>Laravel and Angular <b>Double</b> Page Application</h2>
					<h4>Utenti System</h4>
				</div>

				<div class="col-lg-8">

				<!-- NEW UTENTI FORM -->
				<form ng-submit="submitUtente()"> <!-- ng-submit will disable the default form action and use our function -->
					<!-- Username -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="username" ng-model="utentiData.username" placeholder="Username">
					</div>

					<!-- Password -->
					<div class="form-group">
						<input type="password" class="form-control input-lg" name="password" ng-model="utentiData.password" placeholder="Your strong password">
					</div>

					<!-- SUBMIT -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Create user</button>
					</div>
				</form>
			</div>

			<div class="col-lg-4">
				<pre>
				<% utentiData %>
				</pre>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8">

				<!-- LOADING ICON -->
				<!-- show loading icon if the loading variable is set to true -->
				<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

				<!-- THE USERS -->
				<!-- hide these users if the loading variable is true -->

				<table class="table table-striped utenti">
					<tr ng-hide="loading" ng-repeat="user in users">
						<td><h3><a ng-click="showUser(user.id)" >#<% user.id %><a><small>by <% user.username %></h3></td>
						<td><p><a href="#" ng-click="deleteUser(user.id)" class="text-muted">Delete</a></p></td>
					</tr>
				</table>

			</div>
			<div class="col-lg-4 col-md-4">
				<form ng-submit="updateUser(singleuser)"> <!-- ng-submit will disable the default form action and use our function -->

					<!-- USERNAME -->
					<div class="form-group">
						<input type="text" class="form-control input-sm" name="edit_username" ng-model="singleuser.username" placeholder="Username">
					</div>

					<!-- PASSWORD -->
					<div class="form-group">
						<input type="password" class="form-control input-lg" name="edit_password" ng-model="singleuser.password" placeholder="your strong password">
					</div>

					<!-- SUBMIT -->
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary btn-lg">Save</button>
					</div>
				</form>

			</div>
		</div>
	</div>
@stop
