<?php

class LinksAngularController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $data['links_lista'] = Link::all();
		// $this->layout->content = View::make('links.index', $data);


		$data['links_lista'] = Link::all();
		// var_dump($data['links_lista']);
		foreach($data['links_lista'] as $curr_link) {
			$data['tag_lista'] = $curr_link->tags;
		}

		return Response::json($data['links_lista']);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// $data['tag_lista'] = Tag::all();
 		// $this->layout->content = View::make('links.create', $data);

		return Response::json(Tag::get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = array(
			'nome_link' => Input::get('nome_link')
			,'url' => Input::get('url')
			,'tag.0' => Input::get('tag.0')
		);

		$links = new Link;

		if ( $link->valida($data) == true ) {
			$link->inserisciDati($data);

			return Redirect::action('LinksController@index');
		} else {
			return Redirect::action('LinksController@create')->withInput();
		}


		// FAT MODEL THIN CONTROLLER!
		// $regole = array(
		// 	'nome_link' => 'required'
		// 	,'url' => 'url'
		// 	,'tag.0' => 'required'
		// );
		//
		// $validatore = Validator::make($data,$regole);
		//
		// if( $validatore->passes() ){
		//
		// 	$links->nome_link = $data['nome_link'];
		// 	$links->url = $data['url'];
		//  	$links->save();
		// 	$link_id = $links->id;
		//
		// 	$tag = NULL;
		// 	$quanti_tag = count(Input::get('tag'));
		// 	for($i=0;$i<$quanti_tag;$i++){
		// 		$tag_id  = Input::get('tag.' . $i) . ',';
		// 		$lt = new LinkTag;
		// 		$lt->link_id = $link_id;
		// 		$lt->tag_id = $tag_id;
		// 		$lt->save();
		// 	}
		//
		// }else{
 		//
		// }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['link_dettaglio'] = Link::find($id);
		$this->layout->content = View::make('links.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['link_dettaglio'] = Link::find($id);
		$data['tag_lista'] = $data['link_dettaglio']->tags;
		 ;
		$this->layout->content = View::make('links.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
			'nome_link' => Input::get('nome_link')
			,'url' => Input::get('url')
		);

		$regole = array(
			'nome_link' => 'required'
			,'url' => 'url'
		);

		$validatore = Validator::make($data,$regole);

		if( $validatore->passes() ){
			 $link = Link::find($id);
			 $link->nome_link = $data['nome_link'];
			 $link->url = $data['url'];
			 $link->save();
			 return Redirect::action('LinksController@index');
		}else{
			return Redirect::action('LinksController@edit', [$id])->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	 $link = Link::find($id);
 	 $link->delete();
	 return Redirect::action('LinksController@index');
	}

}
