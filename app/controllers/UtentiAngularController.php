<?php

class UtentiAngularController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		// $data['utenti_lista'] = Cache::remember('utenti_lista', 20, function(){
		// 	return Utenti::all();
		// });
		//
		// $this->layout->content = View::make('utenti.index', $data);

		return Response::json(UtentiAngular::get());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	// public function create()
	// {
	// 	//
	// 	$data['livelli'] = array(
	// 		0 => 'Admin',
	// 		1 => 'Utente Base'
	// 	);
	//
	// 	$data['categorie_lista'] = UtentiAngular::all();
	// 	$this->layout->content = View::make('utenti.create', $data);
	// }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//

		$data = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'categorie.0' => Input::get('categorie.0'),
			'livello' => Input::get('livello')
		);

		$regole = array(
			'username' => 'alpha_num|required',
			'password' => 'alpha_num|required',
			'categorie.0' => 'required',
			'livello' => 'required'
		);

		$validatore = Validator::make($data,$regole);

		if ( $validatore->passes() ) {
			$utenti = new Utenti;
			$utenti->username = $data['username'];
			$utenti->password = Hash::make($data['password']);
			$utenti->livello = $data['livello'];

			$categorie = NULL;
			$quante_categorie = count(Input::get('categorie'));

			for($i=0;$i<$quante_categorie;$i++){
				$categorie .= Input::get('categorie.' . $i) . ',';
			}

			$utenti->categorie = $categorie;
			$utenti->save();
			// Cache::forget('utenti_lista');

			return Redirect::action('UtentiController@index');
		} else {
			return Redirect::action('UtentiController@create')->withInput();
		}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$data['utente_dettaglio'] = Utenti::find($id);
		$this->layout->content = View::make('utenti.show', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		try{
			$data['utenti_dettaglio'] = Utenti::TrovaOLanciaEccezione($id);
			$data['categorie_lista'] = Categorie::all();
			$data['categorie_scelte'] = explode(',', $data['utenti_dettaglio']->categorie);

			$data['livelli'] = array(
					0 => 'Admin',
					1 => 'Utente Base'
			);

			$this->layout->content = View::make('utenti.edit', $data);
		} catch (ModelNonTrovatoEccezione $e) {
			return $e->getMessage();
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//

		$data = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'categorie.0' => Input::get('categorie.0'),
			'livello' => Input::get('livello')
		);

		$regole = array(
			'username' => 'alpha_num|required',
			'categorie.0' => 'required',
			'livello' => 'required'
		);

		$validatore = Validator::make($data, $regole);

		if ( $validatore->passes() ) {
			$utente = Utenti::find($id);
			$utente->username = $data['username'];

			if (Input::has('password')) {
				$utente->password = Hash::make($data['password']);
			}

			$utente->livello = $data['livello'];

			$categorie = NULL;
			$quante_categorie = count(Input::get('categorie'));

			for($i=0; $i < $quante_categorie; $i++) {
						$categorie .= Input::get('categorie.' . $i) . ',';
			}

			$utente->categorie = $categorie;
			$utente->save();
			// Cache::forget('utenti_lista');

			return Redirect::action('UtentiController@index');
		}	else {
			return Redirect::action('UtentiController@edit', [$id])->withInput();
		}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$utente = Utenti::find($id);
		$utente->delete();
		// Cache::forget('utenti_lista');

		return Redirect::action('UtentiController@index');
	}


}
