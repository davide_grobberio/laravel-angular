<?php

class HomeController extends BaseController {

	public function showWelcome()
	{
  		$this->layout->content = View::make('benvenuto');
	}

	public function area_risorse_utenti()
	{
		// return 'Benvenuto nella tua personalissima area utenti';
		$data['categorie'] = explode(',', Auth::user()->categorie);

		$data['risorse'] = Risorse::all();

		$this->layout->content = View::make('area.main_utenti', $data);
	}


	public function login()
	{
		$this->layout->content = View::make('login.form_login');
	}

	public function login_process()
	{

		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);

		if( Auth::attempt($userdata) ){
			Session::put('username', $userdata['username']);

			if (Auth::user()->livello == 0) {
					return Redirect::to('area_utenti');
			} else {
					return Redirect::to('area_risorse_utenti');
			}

		}else{
			return Redirect::to('login');
		}
	}

	public function area_utenti()
	{

		$this->layout->content = View::make('area.main');
	}

	public function showComments()
	{
		$this->layout->content = View::make('comments');
	}

	public function showPosts()
	{
		$this->layout->content = View::make('posts');
	}

	public function showTags()
	{
		$this->layout->content = View::make('tags');
	}

	public function showLinks()
	{
		$this->layout->content = View::make('links');
	}

	public function showCategorie()
	{
		$this->layout->content = View::make('categorie.cat_an');
	}

	public function showUtenti()
	{
		$this->layout->content = View::make('utenti');
	}

}
