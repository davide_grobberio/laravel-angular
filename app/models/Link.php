<?php

class Link extends Eloquent {
		protected $guarded = array();

    protected $table = 'link';

		private $errors;

		private $rules = array(
			'nome_link' => 'required',
			'url' 			=> 'url',
			'tag.0' 		=> 'required'
		);


    public function tags()
    {
    	return $this->belongsToMany('Tag');
    }


		public function valida($data){
			$v = Validator::make($data, $this->rules);

			if ( $v->fails() ){
				$this->errors = $v->errors();
				return false;
			} else {
				return true;
			}
		}


		public function errors(){
			return $this->errors;
		}


		public function inserisciDati($data){
			$this->nome_link = $data['nome_link'];
			$this->url = $data['url'];
			self::save();

			$link_id = $this->id;

			$tag = NULL;

			$quanti_tag = count(Input::get('tag'));
			for($i=0;$i<$quanti_tag;$i++){
				$tag_id = Input::get('tag.' . $i) . ',';

				$lt = new LinkTag;
				$lt->link_id = $link_id;
				$lt->tag_id = $tag_id;
				$lt->save();
			}

		}
}
